# isbnlib-feedstock<br>
[![install with conda](https://anaconda.org/bluehood/isbnlib/badges/installer/conda.svg)](https://anaconda.org/bluehood/isbnlib)
[![last_built](https://anaconda.org/bluehood/isbnlib/badges/latest_release_date.svg)](https://anaconda.org/bluehood/isbnlib)
[![license](https://anaconda.org/bluehood/isbnlib/badges/license.svg)](https://anaconda.org/bluehood/isbnlib)

I am not related to arxiv2bib's development in any way, I just created a conda package for it.
Refer to the [project homepage](https://github.com/xlcnd/isbnlib) for questions or bug reports.

## Conda installation
```bash
$ conda install -c bluehood isbnlib
```
